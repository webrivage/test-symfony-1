Launch Dockerized Mysql database server

    docker-compose up -d

Create database 

    bin/console doctrine:migrations:migrate
    
Load fixtures

    bin/console doctrine:fixtures:load

Launch PHP Http server

    bin/console server:run
    
Open http://127.0.0.1:8000 in your browser
