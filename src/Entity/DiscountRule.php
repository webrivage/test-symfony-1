<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscountRuleRepository")
 */
class DiscountRule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $rule_expression;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $discount_percent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRuleExpression(): ?string
    {
        return $this->rule_expression;
    }

    public function setRuleExpression(string $rule_expression): self
    {
        $this->rule_expression = $rule_expression;

        return $this;
    }

    public function getDiscountPercent()
    {
        return $this->discount_percent;
    }

    public function setDiscountPercent($discount_percent): self
    {
        $this->discount_percent = $discount_percent;

        return $this;
    }
}
