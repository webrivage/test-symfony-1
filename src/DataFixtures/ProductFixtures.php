<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $product = new Product();
        $product->setName('Cafetière');
        $product->setPrice(150.50);
        $product->setType('Electro-ménager');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Enceinte Bluetooth');
        $product->setPrice(100);
        $product->setType('Hi-fi');
        $manager->persist($product);

        $manager->flush();
    }
}
