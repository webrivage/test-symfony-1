<?php

namespace App\DataFixtures;

use App\Entity\DiscountRule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DiscountRuleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rule = new DiscountRule();
        $rule->setRuleExpression('product.type == \'Electro-ménager\' and product.price >= 100');
        $rule->setDiscountPercent(20);
        $manager->persist($rule);

        $rule = new DiscountRule();
        $rule->setRuleExpression('product.type == \'Hi-fi\' and product.price < 100');
        $rule->setDiscountPercent(10);
        $manager->persist($rule);

        $rule = new DiscountRule();
        $rule->setRuleExpression('product.type == \'Cuisine\'');
        $rule->setDiscountPercent(10);
        $manager->persist($rule);

        $manager->flush();
    }
}
