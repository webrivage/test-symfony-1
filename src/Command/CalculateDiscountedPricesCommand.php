<?php

namespace App\Command;

use App\Repository\DiscountRuleRepository;
use App\Repository\ProductRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class CalculateDiscountedPricesCommand extends Command
{
    protected static $defaultName = 'CalculateDiscountedPrices';

    /**
     * @var DiscountRuleRepository
     */
    private $ruleRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CalculateDiscountedPricesCommand constructor.
     *
     * @param DiscountRuleRepository $ruleRepository
     * @param ProductRepository      $productRepository
     */
    public function __construct(DiscountRuleRepository $ruleRepository, ProductRepository $productRepository)
    {
        parent::__construct();

        $this->ruleRepository = $ruleRepository;
        $this->productRepository = $productRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Apply all rules to all products to (re)calculate discounted prices')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        // Load all rules
        $rules = $this->ruleRepository->findAll();

        $expressionLanguage = new ExpressionLanguage();

        // Process all products
        foreach ($this->productRepository->findAll() as $product) {
            $product->setDiscountedPrice(null);

            foreach ($rules as $rule) {
                $valuesObject = new \stdClass();
                $valuesObject->type = $product->getType();
                $valuesObject->price = $product->getPrice();

                $ruleMatch = $expressionLanguage->evaluate(
                    $rule->getRuleExpression(),
                    [
                        'product' => $valuesObject
                    ]
                );

                if (true === $ruleMatch) {
                    $discountedPrice = $product->getPrice() - ($product->getPrice() * $rule->getDiscountPercent() / 100);

                    $product->setDiscountedPrice($discountedPrice);
                }
            }

            $this->productRepository->save($product);
        }

        // TODO: send report email

        $io->success('Discounted prices recalculated !');
    }
}
